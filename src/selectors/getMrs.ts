import {
    getHasMrAlreadyRequestedReview,
} from '../db/requestedReview';
import { Mr } from '../models/Mr';

import {
    getPipelineStatus,
    getMrHasNecessaryApprovals,
    getIsMrEligibleToRequestFollowupReview,
} from './helpers';

export const getMrsToRebase = (mrs: Mr[]) => (
    mrs.filter(mr => (
        mr.diverged_commits_count > 0
        && !mr.rebase_in_progress
        && !mr.work_in_progress
        && !mr.labels.includes('Status::WIP')
        && !mr.has_conflicts
    ))
);

export const getMrsToMerge = (mrs: Mr[]) => {
    // Only get 1 MR to merge at a time due to rebasing constraints
    const mrToMerge = mrs.find(mr => (
        mr.merge_status === 'can_be_merged'
        && mr.diverged_commits_count === 0
        && !mr.rebase_in_progress
        && !mr.work_in_progress
        && !mr.labels.includes('DO NOT MERGE')
        && !mr.labels.includes('No Automerge')
        && mr.labels.includes('Status::Ready')
        && getPipelineStatus(mr) === 'success'
        && !mr.has_conflicts
        && mr.blocking_discussions_resolved
        && getMrHasNecessaryApprovals(mr)
    ));

    if (mrToMerge) { return [mrToMerge]; }
    return [];
};

export const getMrsToMarkAsWip = (mrs: Mr[]) => (
    mrs.filter(mr => {
        const isAlreadyWip = mr.labels.includes('Status::WIP');
        if (isAlreadyWip) { return false; }

        const isPipelineFailing = (
            getPipelineStatus(mr) === 'failed'
        );
        if (isPipelineFailing) { return true; }

        const hasConflicts = mr.has_conflicts;
        if (hasConflicts) { return true; }

        const hasRebaseFailed = (
            mr.merge_error === 'Rebase failed. Please rebase locally'
        );
        if (hasRebaseFailed) { return true; }

        return false;
    })
);

export const getMrsToMarkAsInDiscussion = (mrs: Mr[]) => (
    mrs.filter(mr => (
        !mr.labels.includes('Status::InDiscussion')
        && !mr.labels.includes('Status::WIP')
        && !mr.work_in_progress
        && !mr.blocking_discussions_resolved
    ))
);

export const getMrsToMarkAsReady = (mrs: Mr[]) => (
    mrs.filter(mr => (
        !mr.labels.includes('Status::Ready')
        && !mr.labels.includes('Status::ReadyForQA')
        && !mr.labels.includes('Status::WIP')
        && !mr.work_in_progress
        && mr.blocking_discussions_resolved
    ))
);

export const getMrsToRequestInitialReview = (mrs: Mr[]) => (
    mrs.filter(mr => (
        (
            mr.labels.includes('Status::Ready')
            || mr.labels.includes('Status::InDiscussion')
        )
        && !mr.work_in_progress
        && getPipelineStatus(mr) === 'success'
        && !mr.has_conflicts
        && !getMrHasNecessaryApprovals(mr)
        && !getHasMrAlreadyRequestedReview(mr)
    ))
);

export const getMrsToRequestFollowupReview = (mrs: Mr[]) => (
    mrs.filter(mr => (
        (
            mr.labels.includes('Status::Ready')
            || mr.labels.includes('Status::InDiscussion')
        )
        && !mr.work_in_progress
        && getPipelineStatus(mr) === 'success'
        && !mr.has_conflicts
        && !getMrHasNecessaryApprovals(mr)
        && getHasMrAlreadyRequestedReview(mr)
        && getIsMrEligibleToRequestFollowupReview(mr)
    ))
);
