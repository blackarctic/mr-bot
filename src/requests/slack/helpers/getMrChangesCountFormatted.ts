import { Mr } from '../../../models/Mr';

export const getMrChangesCountFormatted = (mr: Mr) => {
    const count = mr.changes_count;
    const units = count === '1' ? 'file' : 'files';
    return `${count} ${units}`;
};
