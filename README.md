# Mr Bot

A bot to assist with the mundane tasks of MR babysitting, like rebasing, messaging team members for approvals, and monitoring the status of your MR pipeline.

One repo per instance of Mr Bot. (A future improvement would be multiple repos managed via one instance of Mr Bot.) 

## Getting Started

1. Use correct Node version

```sh
nvm install && nvm use
```

2. Install dependencies

```sh
yarn
```

3. Copy `.env.template` to a new `.env` file and populate it with your custom values.

4. Start it up

```
yarn start
```

## Running via PM2

This is really meant to be run on a server via [PM2](https://pm2.keymetrics.io/docs/usage/quick-start/).

Use the scripts in `scripts/pm2` to `start`, `stop`, and `update` on your server. Be sure to create a `scripts/pm2/.env` file with a `NAME` value for pm2 to use.
