import { fetchAllOpenMrs } from "./fetchMrs";
import { executeGitlabReq } from "./helpers";
import { env } from "../../env";

export const clearRenovateBotMrsApprovals = async () => {
    if (!env.ENABLE_RENOVATE_BOT) { return; }
    const mrs = await fetchAllOpenMrs(['RenovateBot: Automerge']);
    const mrsToClearApprovals = mrs.filter(
        mr => mr.approval_state.rules.some(rule => rule.approvals_required)
    );
    const mrsToClearApprovalsPromises = mrsToClearApprovals.map(mrToClearApprovals => {
        executeGitlabReq({
            method: 'post',
            path: `/${mrToClearApprovals.iid}/approvals`,
            params: {
                approvals_required: 0,
            },
        });
    });
    await Promise.all(mrsToClearApprovalsPromises);
    console.log('=========');
    console.log('Processing RenovateBot MRs...');
    console.log('mrsToClearApprovals', mrsToClearApprovals.map(x => x.title));
    console.log('Completed processing RenovateBot MRs. Resuming...');
    console.log('=========');
};
