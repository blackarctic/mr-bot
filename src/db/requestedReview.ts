import { db } from './db';
import { Mr } from '../models/Mr';

export const getWhenHasMrRequestedReview = (mr: Mr) => {
    const savedMr = db.get(`mrs.${mr.iid}`).value();
    return savedMr && savedMr.requestedReviewAt;
};

export const getHasMrAlreadyRequestedReview = (mr: Mr) => {
    return !!(getWhenHasMrRequestedReview(mr));
};

export const saveWhenMrRequestedReview = (mr: Mr, when = Date.now()) => {
    const savedMr = db.get(`mrs.${mr.iid}`).value() || {};
    savedMr.requestedReviewAt = when;
    db.set(`mrs.${mr.iid}`, savedMr).write();
};
