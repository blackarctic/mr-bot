import { sendMessage } from '../slack/sendMessage';
import { Mr } from '../../models/Mr';

import {
    executeGitlabReq,
    getLabelsWithoutStatusPrefix,
    makeUpdateMrs,
} from './helpers';

const markMrAsInDiscussion = async (mr: Mr) => {
    await executeGitlabReq({
        params: {
            labels: [
                ...getLabelsWithoutStatusPrefix(mr.labels),
                'Status::InDiscussion',
            ],
        },
        path: `/${mr.iid}`,
        method: 'put',
    });

    const statusMessage = 'has been marked as "In Discussion" until all discussions are resolved';
    await sendMessage(`🟡 <${mr.web_url}|!${mr.iid} ${mr.title}> ${statusMessage}.`, `@${mr.author.username}`);
};

const markMrAsWip = async (mr: Mr) => {
    await executeGitlabReq({
        params: {
            labels: [
                ...getLabelsWithoutStatusPrefix(mr.labels),
                'Status::WIP',
            ],
        },
        path: `/${mr.iid}`,
        method: 'put',
    });

    const statusMessage = 'has been marked as "WIP" due to issues. Please mark as "Ready" when issues have been addressed';
    await sendMessage(`🔴 <${mr.web_url}|!${mr.iid} ${mr.title}> ${statusMessage}.`, `@${mr.author.username}`);
};

const markMrAsReady = async (mr: Mr) => {
    await executeGitlabReq({
        params: {
            labels: [
                ...getLabelsWithoutStatusPrefix(mr.labels),
                'Status::Ready',
            ],
        },
        path: `/${mr.iid}`,
        method: 'put',
    });

    const statusMessage = 'has been marked as "Ready"';
    await sendMessage(`🟢 <${mr.web_url}|!${mr.iid} ${mr.title}> ${statusMessage}.`, `@${mr.author.username}`);
};

const rebaseMr = async (mr: Mr) => {
    await executeGitlabReq({
        path: `/${mr.iid}/rebase`,
        method: 'put',
    });
};

const mergeMr = async (mr: Mr) => {
    try {
        await executeGitlabReq({
            params: {
                should_remove_source_branch: true,
                squash: true,
            },
            path: `/${mr.iid}/merge`,
            method: 'put',
        });
    } catch (error) {
        // Don't reject on 405
        // Sometimes we have a race condition where we deem
        // an MR as merge-able but by the time we attempt
        // to merge it, it isn't merge-able anymore. This is ok.
        if (
            error
            && error.response
            && error.response.status
            && error.response.status === 405
        ) {
            console.log(`MR !${mr.iid} was not able to be merged. Received 405 response status.`);
            return;
        }
        // Otherwise, this is a valid error to throw
        throw error;
    }

    const statusMessage = 'has been merged';
    await sendMessage(`🟣 <${mr.web_url}|!${mr.iid} ${mr.title}> ${statusMessage}.`, `@${mr.author.username}`);
};

export const markMrsAsInDiscussion = makeUpdateMrs(markMrAsInDiscussion);
export const markMrsAsWip = makeUpdateMrs(markMrAsWip);
export const markMrsAsReady = makeUpdateMrs(markMrAsReady);
export const rebaseMrs = makeUpdateMrs(rebaseMr);
export const mergeMrs = makeUpdateMrs(mergeMr);
