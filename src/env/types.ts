export type MdyDate = {
    month: number; // 1-12
    date: number; // 1-31
    year: number; // 0-99
}

export type InjectedEnv = {
    GITLAB_HOST?: string;
    GITLAB_PROJECT_ID?: string;
    GITLAB_TOKEN?: string;

    SLACK_WEBHOOK_URL?: string;
    SLACK_ERROR_MONIT_CHANNEL?: string;
    SLACK_REVIEW_REQ_CHANNEL?: string;
    SLACK_REVIEW_REQ_REVIEWERS?: string;

    TZ?: string;

    AM_REMINDER_HOUR?: string;
    AM_REMINDER_MIN?: string;
    PM_REMINDER_HOUR?: string;
    PM_REMINDER_MIN?: string;

    ENABLE_RENOVATE_BOT?: string;
    ENABLE_WEEKEND_REVIEW_REQUESTS?: string;

    NO_REVIEW_REQUESTS_ON_THESE_DATES?: string;

    MIN_HOURS_BETWEEN_REVIEW_REQUESTS?: string;

    INTERVAL_SECONDS?: string;
    FAIL_RETRY_ATTEMPTS?: string;
}

export type Env = {
    GITLAB_HOST: string;
    GITLAB_PROJECT_ID: number;
    GITLAB_TOKEN: string;

    SLACK_WEBHOOK_URL: string;
    SLACK_ERROR_MONIT_CHANNEL: string;
    SLACK_REVIEW_REQ_CHANNEL: string;
    SLACK_REVIEW_REQ_REVIEWERS: string[];

    AM_REMINDER_HOUR: number | null;
    AM_REMINDER_MIN: number | null;
    PM_REMINDER_HOUR: number | null;
    PM_REMINDER_MIN: number | null;

    ENABLE_RENOVATE_BOT: boolean;
    ENABLE_WEEKEND_REVIEW_REQUESTS: boolean;

    NO_REVIEW_REQUESTS_ON_THESE_DATES: MdyDate[];

    MIN_HOURS_BETWEEN_REVIEW_REQUESTS: number;

    INTERVAL_SECONDS: number;
    FAIL_RETRY_ATTEMPTS: number;
}
