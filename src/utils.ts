import ms from 'ms';
import queryString from 'query-string';

import { Dictionary } from './types';

export const stringifyParams = (params: Dictionary) => (
    queryString.stringify(params, {
        arrayFormat: 'comma',
    })
);

export const convertMsToHrs = (timeInMs: number) => {
    const timeInSec = timeInMs / 1000;
    const timeInMin = timeInSec / 60;
    const timeInHrs = timeInMin / 60;
    return timeInHrs;
};

export const getStringsWithoutPrefix = (strings: string[], prefix: string) => (
    strings.filter(x => !x.startsWith(prefix))
);

export const getAllExcept = (allItems: any[], itemsToExclude: any[]) => (
    allItems.filter(x => !itemsToExclude.includes(x))
);

export const rejectAfterMs = (
    numberOfMs: number,
    errorMessage?: string
): Promise<void> => {
    return new Promise((_resolve, reject) => {
        setTimeout(() => {
            reject(
                new Error(errorMessage || `Timeout of ${ms(numberOfMs)} reached.`)
            );
        }, numberOfMs);
    });
};

export const withRejectTimeout = async (
    promise: Promise<any>,
    name: string,
    numberOfMs: number
) => {
    const errorMessage = `${name} failed to complete after ${ms(numberOfMs)}.`;
    await Promise.race([rejectAfterMs(numberOfMs, errorMessage), promise]);
};
