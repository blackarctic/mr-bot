import { fetchAllOpenMrs } from './requests/gitlab/fetchMrs';
import { cleanUpDb, saveWhenRunCompleted } from './db';
import { getAllExcept } from './utils';
import {
    getAreReviewRequestsEnabledToday,
    getShouldRequestAnyFollowupReviews,
} from './requests/slack/helpers';
import {
    getMrsToMerge,
    getMrsToRebase,
    getMrsToMarkAsWip,
    getMrsToMarkAsInDiscussion,
    getMrsToMarkAsReady,
    getMrsToRequestInitialReview,
    getMrsToRequestFollowupReview,
} from './selectors';
import {
    markMrsAsInDiscussion,
    markMrsAsWip,
    markMrsAsReady,
    rebaseMrs,
    mergeMrs,
} from './requests/gitlab/updateMrs';
import {
    clearRenovateBotMrsApprovals,
} from './requests/gitlab/renovateBotMrs';
import {
    requestInitialReviewOnMrs,
    requestFollowupReviewOnMrsAtTimes,
} from './requests/slack/requestReviewOnMrs';

export const run = async () => {
    await clearRenovateBotMrsApprovals();
    const mrs = await fetchAllOpenMrs();

    cleanUpDb(mrs);

    const areReviewRequestsEnabledToday = getAreReviewRequestsEnabledToday({
        shouldDisplayDisabledNotice: true,
    });
    const shouldRequestFollowupReviews = getShouldRequestAnyFollowupReviews();

    const mrsToMerge = getMrsToMerge(mrs);
    const mrsToRebase = getMrsToRebase(mrs);
    const mrsToRequestInitialReview = areReviewRequestsEnabledToday
        ? getMrsToRequestInitialReview(mrs)
        : null;
    const mrsToRequestFollowupReview = shouldRequestFollowupReviews
        ? getMrsToRequestFollowupReview(mrs)
        : null;
    const mrsToMarkAsWip = getMrsToMarkAsWip(mrs);
    const mrsToMarkAsInDiscussion = getMrsToMarkAsInDiscussion(
        // Marking as "WIP" takes precedence
        getAllExcept(mrs, mrsToMarkAsWip),
    );
    const mrsToMarkAsReady = getMrsToMarkAsReady(
        // Marking as "WIP" or "In Discussion" takes precedence
        getAllExcept(mrs, [...mrsToMarkAsWip, ...mrsToMarkAsInDiscussion]),
    );

    console.log('mrsToMerge', mrsToMerge.map(x => x.title));
    console.log('mrsToRebase', mrsToRebase.map(x => x.title));
    console.log('mrsToRequestInitialReview', (
        mrsToRequestInitialReview ? mrsToRequestInitialReview.map(x => x.title) : '(skipped)'
    ));
    console.log('mrsToRequestFollowupReview', (
        mrsToRequestFollowupReview ? mrsToRequestFollowupReview.map(x => x.title) : '(skipped)'
    ));
    console.log('mrsToMarkAsWip', mrsToMarkAsWip.map(x => x.title));
    console.log('mrsToMarkAsInDiscussion', mrsToMarkAsInDiscussion.map(x => x.title));
    console.log('mrsToMarkAsReady', mrsToMarkAsReady.map(x => x.title));

    const promises = [
        markMrsAsReady(mrsToMarkAsReady),
        markMrsAsInDiscussion(mrsToMarkAsInDiscussion),
        markMrsAsWip(mrsToMarkAsWip),
        rebaseMrs(mrsToRebase),
        mergeMrs(mrsToMerge),
        mrsToRequestInitialReview
            ? requestInitialReviewOnMrs(mrsToRequestInitialReview)
            : Promise.resolve(),
        mrsToRequestFollowupReview
            ? requestFollowupReviewOnMrsAtTimes(mrsToRequestFollowupReview)
            : Promise.resolve(),
    ];
    await Promise.all(promises);

    saveWhenRunCompleted();
}
