import { env } from '../env';
import { Mr } from '../models/Mr';
import { convertMsToHrs } from '../utils';
import { getWhenHasMrRequestedReview } from '../db/requestedReview'

export const getPipelineStatus = (mr: Mr) => {
    if (mr.head_pipeline) {
        return mr.head_pipeline.status;
    }
    if (mr.pipeline) {
        return mr.pipeline.status;
    }
    return null;
};

export const getMrHasNecessaryApprovals = (mr: Mr) => {
    const { approval_state } = mr;
    if (!approval_state) { return false; }

    const { rules } = approval_state;
    if (!rules) { return false; }

    return rules.every(rule => {
        const { approvals_required, approved_by } = rule;
        const approvals_count = approved_by.length;
        return approvals_count >= approvals_required;
    });
};

export const getIsMrEligibleToRequestFollowupReview = (mr: Mr) => {
    const whenMrLastRequestedReviewMs = getWhenHasMrRequestedReview(mr);
    if (!whenMrLastRequestedReviewMs) { return false; }
    const whenMrLastRequestedReviewHrs = convertMsToHrs(whenMrLastRequestedReviewMs);
    const nowHrs = convertMsToHrs(Date.now());
    const hrsHavePassedSinceLastReviewRequest = nowHrs - whenMrLastRequestedReviewHrs;
    const minHoursBetweenReviewRequests = env.MIN_HOURS_BETWEEN_REVIEW_REQUESTS;
    return hrsHavePassedSinceLastReviewRequest > minHoursBetweenReviewRequests;
};
