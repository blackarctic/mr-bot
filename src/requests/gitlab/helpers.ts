import axios from 'axios';

import { env } from '../../env';
import { Mr } from '../../models/Mr';
import { Dictionary } from '../../types';
import { stringifyParams, getStringsWithoutPrefix } from '../../utils';

const {
    GITLAB_HOST,
    GITLAB_PROJECT_ID,
    GITLAB_TOKEN,
} = env;

const GITLAB_API_URL = `${GITLAB_HOST}/api/v4`;
const GITLAB_PROJECTS_URL = `${GITLAB_API_URL}/projects/${GITLAB_PROJECT_ID}`;
const GITLAB_MR_URL = `${GITLAB_PROJECTS_URL}/merge_requests`;

const GITLAB_AUTH_PARAMS = {
    access_token: GITLAB_TOKEN
};

const createGitlabReqParams = (params: Dictionary) => ({
    ...params,
    ...GITLAB_AUTH_PARAMS,
});

export const executeGitlabReq = async ({
    params = {},
    path = '',
    method = 'get',
}: {
    params?: Dictionary;
    path?: string;
    method?: 'get' | 'post' | 'put' | 'delete';
} = {}) => {
    const stringifiedParams = stringifyParams(createGitlabReqParams(params));
    const url = `${GITLAB_MR_URL}${path}?${stringifiedParams}`;
    if (method === 'get') {
        return await axios.get(url);
    }
    if (method === 'post') {
        return await axios.post(url);
    }
    if (method === 'put') {
        return await axios.put(url);
    }
    if (method === 'delete') {
        return await axios.delete(url);
    }
    throw new Error(`executeGitlabReq method "${method}" is not valid. Expected "get" or "put".`)
};

export const getLabelsWithoutStatusPrefix = (labels: string[]) => (
    getStringsWithoutPrefix(labels, 'Status::')
);

export const makeUpdateMrs = (
    fn: (mr: Mr) => Promise<void>,
) => async (mrs: Mr[]) => {
    const promises = mrs.map(mr => fn(mr));
    await Promise.all(promises);
};
