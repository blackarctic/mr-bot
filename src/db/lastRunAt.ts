import { db } from './db';

export const saveWhenRunCompleted = (when = Date.now()) => {
    db.set('lastRunAt', when).write();
};
