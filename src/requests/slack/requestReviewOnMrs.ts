import { Mr } from '../../models/Mr';
import { makeUpdateMrs } from '../gitlab/helpers'
import { saveWhenMrRequestedReview } from '../../db/requestedReview';
import {
    saveReviewReminderAmSent,
    saveReviewReminderPmSent,
} from '../../db/reviewReminders';
import { env } from '../../env';

import { sendFormattedMessage } from './sendMessage';
import {
    getMrChangesCountFormatted,
    getShouldRequestAmFollowupReviews,
    getShouldRequestPmFollowupReviews,
} from './helpers';

const {
    SLACK_REVIEW_REQ_CHANNEL,
    SLACK_REVIEW_REQ_REVIEWERS,
} = env;

const reviewers = SLACK_REVIEW_REQ_REVIEWERS.map(x => `<${x}>`).join(' ');
const reviewersField = {
    value: SLACK_REVIEW_REQ_REVIEWERS.length
        ? `Reviewers: ${reviewers}`
        : '',
    short: false,
};

export const requestInitialReviewOnMr = async (mr: Mr) => {
    const message = [{
        fallback: `Ready For Review: !${mr.iid} (${getMrChangesCountFormatted(mr)})`,
        pretext: `*Ready for Review*: <${mr.web_url}|!${mr.iid}> (${getMrChangesCountFormatted(mr)})`,
        color: '#1CAD46',
        fields: [
            {
                value: `<${mr.web_url}|*${mr.title}*> by <@${mr.author.username}>`,
                short: false,
            },
            reviewersField,
        ],
    }];
    await sendFormattedMessage(message, SLACK_REVIEW_REQ_CHANNEL);
    saveWhenMrRequestedReview(mr);
};

export const requestInitialReviewOnMrs = makeUpdateMrs(requestInitialReviewOnMr);

export const requestFollowupReviewOnMrs = async (mrs: Mr[]) => {
    if (!mrs.length) { return; }
    const mrsCountFormatted = mrs.length > 1 ? `${mrs.length} MRs` : '1 MR';
    const mrReviewRequestFields = mrs.map(mr => ({
        value: `<${mr.web_url}|*${mr.title}*> by <@${mr.author.username}>`,
        short: false,
    }))
    const message = [{
        fallback: `Still Awaiting Approval: ${mrsCountFormatted}`,
        pretext: `*Still Awaiting Approval*: ${mrsCountFormatted}`,
        color: '#FEB233',
        fields: [
            ...mrReviewRequestFields,
            reviewersField,
        ],
    }];
    await sendFormattedMessage(message, SLACK_REVIEW_REQ_CHANNEL);
    mrs.forEach(mr => saveWhenMrRequestedReview(mr));
};

export const requestFollowupReviewOnMrsAtTimes = async (mrs: Mr[]) => {
    const shouldRequestAmFollowupReviews = getShouldRequestAmFollowupReviews();
    const shouldRequestPmFollowupReviews = getShouldRequestPmFollowupReviews();

    if (shouldRequestAmFollowupReviews && shouldRequestPmFollowupReviews) {
        await requestFollowupReviewOnMrs(mrs);
        saveReviewReminderAmSent();
        saveReviewReminderPmSent();
        return;
    }

    if (shouldRequestAmFollowupReviews) {
        await requestFollowupReviewOnMrs(mrs);
        saveReviewReminderAmSent();
        return;
    }

    if (shouldRequestPmFollowupReviews) {
        await requestFollowupReviewOnMrs(mrs);
        saveReviewReminderPmSent();
        return;
    }
};
