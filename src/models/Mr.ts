export type Author = {
    id: number;
    name: string;
    username: string;
    state: string;
    avatar_url: string;
    web_url: string
};

export type Pipeline = {
    id: number;
    sha: string;
    ref: string;
    status: string;
    web_url: string;
};

export type ApprovalState = {
    approval_rules_overwritten: boolean;
    rules: Array<{
        id: number;
        name: string;
        rule_type: string;
        approvals_required: 2;
        approved_by: [
            {
                id: number;
                name: string;
                username: string;
                state: string;
                avatar_url: string;
                web_url: string
            }
        ];
        approved: boolean
    }>;
};

export type Mr = {
    id: number;
    iid: number;
    project_id: number;
    title: string;
    description: string;
    state: string;
    created_at: string;
    updated_at: string;
    author: Author;
    labels: string[];
    work_in_progress: boolean;
    merge_status: string;
    web_url: string;
    changes_count: string;
    pipeline: Pipeline;
    head_pipeline: Pipeline;
    diverged_commits_count: number;
    rebase_in_progress: boolean;
    has_conflicts: boolean;
    merge_error: string;
    blocking_discussions_resolved: boolean;
    approval_state: ApprovalState;
};
