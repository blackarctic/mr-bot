import { env } from '../../../env';

export const getAreReviewRequestsEnabledToday = ({
    shouldDisplayDisabledNotice = false,
} = {}) => {
    const areWeekendReviewRequestsEnabled = env.ENABLE_WEEKEND_REVIEW_REQUESTS;
    const areWeekendReviewRequestsDisabled = !areWeekendReviewRequestsEnabled;

    const nowDateObj = new Date();

    if (areWeekendReviewRequestsDisabled) {
        const nowDay = nowDateObj.getDay();
        const SUNDAY = 0;
        const SATURDAY = 6;
        const isTodayAWeekendDay = (
            nowDay === SATURDAY || nowDay === SUNDAY
        );
        if (isTodayAWeekendDay) {
            if (shouldDisplayDisabledNotice) {
                console.log('NOTICE: Review requests are disabled today due to ENABLE_WEEKEND_REVIEW_REQUESTS');
            }
            return false;
        }
    }

    const datesToDisableReviewRequests = env.NO_REVIEW_REQUESTS_ON_THESE_DATES;

    const nowMonth = nowDateObj.getMonth() + 1; // 0-11 -> 1-12
    const nowDate = nowDateObj.getDate();
    const nowYear = Number(`${nowDateObj.getFullYear()}`.slice(2)); // 2020 -> 20

    const areReviewRequestsDisabledOnTodaysDate = datesToDisableReviewRequests
        .some(dateToDisableReviewRequests => (
            dateToDisableReviewRequests.month === nowMonth
            && dateToDisableReviewRequests.date === nowDate
            && dateToDisableReviewRequests.year === nowYear
        ));

    if (areReviewRequestsDisabledOnTodaysDate) {
        if (shouldDisplayDisabledNotice) {
            console.log('NOTICE: Review requests are disabled today due to NO_REVIEW_REQUESTS_ON_THESE_DATES');
        }
        return false;
    }

    return true;
};
