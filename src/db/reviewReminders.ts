import { db } from './db';

export const saveReviewReminderAmSent = () => {
    db.set('reviewReminderAmSent', true).write();
};

export const saveReviewReminderPmSent = () => {
    db.set('reviewReminderPmSent', true).write();
};

export const getHasReviewReminderAmBeenSent = () => {
    return db.get('reviewReminderAmSent').value();
};

export const getHasReviewReminderPmBeenSent = () => {
    return db.get('reviewReminderPmSent').value();
};
