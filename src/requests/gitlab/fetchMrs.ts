import { Mr, ApprovalState } from '../../models/Mr';

import { executeGitlabReq } from './helpers';

const fetchAllOpenMrIds = async (labels: string[]): Promise<string[]> => {
    const response = await executeGitlabReq({
        params: {
            state: 'opened',
            labels,
        },
    });
    const { data } = response;
    if (!data) {
        throw new Error('No data found for fetchAllOpenMrIds');
    }
    return data.map((x: { iid: string }) => x.iid);
};

const fetchMr = async (id: string): Promise<Mr> => {
    const response = await executeGitlabReq({
        params: {
            include_diverged_commits_count: true,
            include_rebase_in_progress: true,
        },
        path: `/${id}`,
    });
    const { data } = response;
    if (!data) {
        throw new Error(`No data found for fetchMr of id "${id}"`);
    }
    return data;
};

const fetchMrApprovals = async (id: string): Promise<ApprovalState> => {
    const response = await executeGitlabReq({
        path: `/${id}/approval_state`,
    });
    const { data } = response;
    if (!data) {
        throw new Error(`No data found for fetchMrApprovals of id "${id}"`);
    }
    return data;
};

export const fetchAllOpenMrs = async (labels = ['Automate']) => {
    const allOpenMrIds = await fetchAllOpenMrIds(labels);
    const allOpenMrPromises = allOpenMrIds.map(id => fetchMr(id));
    const allOpenMrApprovalsPromises = allOpenMrIds.map(id => fetchMrApprovals(id));
    const allOpenMrs = await Promise.all(allOpenMrPromises);
    const allOpenMrApprovals = await Promise.all(allOpenMrApprovalsPromises);
    allOpenMrs.forEach((openMr, i) => {
        openMr.approval_state = allOpenMrApprovals[i];
    });
    return allOpenMrs;
};
