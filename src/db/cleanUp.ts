import { db } from './db';
import { Dictionary } from '../types';
import { convertMsToHrs } from '../utils';
import { Mr } from '../models/Mr';

const cleanUpMrs = (fetchedMrs: Mr[]) => {
    // Determine the still relevant MRs
    const savedMrs = db.get('mrs').value();
    const savedMrIds = Object.keys(savedMrs);
    const stillRelevantSavedMrIds = savedMrIds.filter(savedMrId => {
        const fetchedMrIds = fetchedMrs.map(mr => `${mr.iid}`);
        const isSavedMrInFetchedMrs = fetchedMrIds.includes(`${savedMrId}`);
        return isSavedMrInFetchedMrs;
    });

    // Populate the new MRs object with the still relevant MRs
    const newMrsObj: Dictionary<Mr> = {};
    stillRelevantSavedMrIds.forEach(savedMrId => {
        newMrsObj[savedMrId] = savedMrs[savedMrId];
    });

    db.set('mrs', newMrsObj).write();
};

const cleanUpReviewReminders = () => {
    // Determine the last run date (1-31)
    const lastRunAt = db.get('lastRunAt').value();
    const lastRunDateObj = new Date(lastRunAt);
    const lastRunDate = lastRunDateObj.getDate();
    const nowDate = new Date().getDate();

    // Determine whether it has been more than 24 hours
    const timeDiffInMs = Date.now() - lastRunAt;
    const timeDiffInHrs = convertMsToHrs(timeDiffInMs);
    const hasBeenMoreThan24Hours = timeDiffInHrs > 24;

    // Clear the reviewReminderSent values upon starting a new day
    const isAtNextDay = lastRunDate !== nowDate || hasBeenMoreThan24Hours;
    if (isAtNextDay) {
        db.set('reviewReminderAmSent', false).write();
        db.set('reviewReminderPmSent', false).write();
    }
};

export const cleanUpDb = (fetchedMrs: Mr[]) => {
    cleanUpReviewReminders();
    cleanUpMrs(fetchedMrs);
};
