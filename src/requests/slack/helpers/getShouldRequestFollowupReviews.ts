import { env } from '../../../env';
import {
    getHasReviewReminderAmBeenSent,
    getHasReviewReminderPmBeenSent,
} from '../../../db/reviewReminders'
import { getAreReviewRequestsEnabledToday } from './getAreReviewRequestsEnabledToday';

export const getShouldRequestFollowupReviews = (amOrPm: 'am' | 'pm') => {
    if (amOrPm !== 'am' && amOrPm !== 'pm') {
        throw new Error('amOrPm requires a value of "am" or "pm"');
    }

    const areReviewRequestsEnabledToday = getAreReviewRequestsEnabledToday();
    if (!areReviewRequestsEnabledToday) { return false; }

    const {
        AM_REMINDER_HOUR,
        AM_REMINDER_MIN,
        PM_REMINDER_HOUR,
        PM_REMINDER_MIN,
    } = env;

    const hr = Number(amOrPm === 'am' ? AM_REMINDER_HOUR : PM_REMINDER_HOUR);
    const min = Number(amOrPm === 'am' ? AM_REMINDER_MIN : PM_REMINDER_MIN);

    if ((hr !== 0 && !hr) || (min !== 0 && !hr)) {
        return false;
    }

    const hasReviewRemindersBeenSent = amOrPm === 'am'
        ? getHasReviewReminderAmBeenSent()
        : getHasReviewReminderPmBeenSent();

    if (hasReviewRemindersBeenSent) {
        return false;
    }

    const now = new Date();
    const nowHr = now.getHours();
    const nowMin = now.getMinutes();

    const isTimeToSendReminder = (
        (nowHr === hr && nowMin >= min) || (nowHr > hr)
    );

    if (isTimeToSendReminder) {
        return true;
    }
    return false;
};

export const getShouldRequestAmFollowupReviews = () => {
    return getShouldRequestFollowupReviews('am');
};

export const getShouldRequestPmFollowupReviews = () => {
    return getShouldRequestFollowupReviews('pm');
};

export const getShouldRequestAnyFollowupReviews = () => (
    getShouldRequestAmFollowupReviews() || getShouldRequestPmFollowupReviews()
);
