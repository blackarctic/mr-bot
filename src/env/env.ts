require('dotenv').config();

import { InjectedEnv, Env, MdyDate } from './types';

const injectedEnv: InjectedEnv = process.env;

const envVarToBool = (s: string | undefined) => (
    !!(s && s.toLowerCase() !== 'false')
);

const envVarToNumberOrNull = (s: string | undefined, varName: string) => {
    if (s === undefined) { return null; }
    const n = Number(s);
    if (Number.isNaN(n)) {
        throw new Error(`${varName} must be a number if set`);
    }
    return n;
}

/* Ensure GITLAB_HOST */

const { GITLAB_HOST } = injectedEnv;

if (!GITLAB_HOST) {
    throw new Error('GITLAB_HOST must be set');
}

/* Ensure GITLAB_PROJECT_ID */

if (!injectedEnv.GITLAB_PROJECT_ID) {
    throw new Error('GITLAB_PROJECT_ID must be set');
}

const GITLAB_PROJECT_ID = Number(injectedEnv.GITLAB_PROJECT_ID);

if (Number.isNaN(GITLAB_PROJECT_ID)) {
    throw new Error('GITLAB_PROJECT_ID must be a number');
}

/* Ensure GITLAB_TOKEN */

const { GITLAB_TOKEN } = injectedEnv;

if (!GITLAB_TOKEN) {
    throw new Error('GITLAB_TOKEN must be set');
}

/* Ensure SLACK_WEBHOOK_URL */

const { SLACK_WEBHOOK_URL } = injectedEnv;

if (!SLACK_WEBHOOK_URL) {
    throw new Error('SLACK_WEBHOOK_URL must be set');
}

/* Ensure SLACK_ERROR_MONIT_CHANNEL */

const { SLACK_ERROR_MONIT_CHANNEL } = injectedEnv;

if (!SLACK_ERROR_MONIT_CHANNEL) {
    throw new Error('SLACK_ERROR_MONIT_CHANNEL must be set');
}

/* Ensure SLACK_REVIEW_REQ_CHANNEL */

const { SLACK_REVIEW_REQ_CHANNEL } = injectedEnv;

if (!SLACK_REVIEW_REQ_CHANNEL) {
    throw new Error('SLACK_REVIEW_REQ_CHANNEL must be set');
}

/* Ensure SLACK_REVIEW_REQ_REVIEWERS */

const SLACK_REVIEW_REQ_REVIEWERS = injectedEnv.SLACK_REVIEW_REQ_REVIEWERS
    ? injectedEnv.SLACK_REVIEW_REQ_REVIEWERS.split(',').map(x => x.trim())
    : [];

/* Ensure TZ */

const { TZ } = injectedEnv;

if (!TZ) {
    throw new Error('TZ must be set');
}

/* Ensure AM/PM_REMINDER_HOUR/MIN vars */

const AM_REMINDER_HOUR = envVarToNumberOrNull(
    injectedEnv.AM_REMINDER_HOUR,
    'AM_REMINDER_HOUR',
);
const AM_REMINDER_MIN = envVarToNumberOrNull(
    injectedEnv.AM_REMINDER_MIN,
    'AM_REMINDER_MIN',
);
const PM_REMINDER_HOUR = envVarToNumberOrNull(
    injectedEnv.PM_REMINDER_HOUR,
    'PM_REMINDER_HOUR',
);
const PM_REMINDER_MIN = envVarToNumberOrNull(
    injectedEnv.PM_REMINDER_MIN,
    'PM_REMINDER_MIN',
);

/* Ensure ENABLE_RENOVATE_BOT */

const ENABLE_RENOVATE_BOT = (
    envVarToBool(injectedEnv.ENABLE_RENOVATE_BOT)
);

/* Ensure ENABLE_WEEKEND_REVIEW_REQUESTS */

const ENABLE_WEEKEND_REVIEW_REQUESTS = (
    envVarToBool(injectedEnv.ENABLE_WEEKEND_REVIEW_REQUESTS)
);

/* Ensure NO_REVIEW_REQUESTS_ON_THESE_DATES */

const NO_REVIEW_REQUESTS_ON_THESE_DATES = (
    (injectedEnv.NO_REVIEW_REQUESTS_ON_THESE_DATES || '')
    .split(',')
    .map(dateString => {
        const parts = dateString.trim().split('/');
        if (parts.length !== 3) {
            throw new Error('NO_REVIEW_REQUESTS_ON_THESE_DATES dates must be formatted as MM/DD/YY');
        }
        const parsedParts = parts.map(part => Number(part));
        const mdy: MdyDate = {
            month: parsedParts[0],
            date: parsedParts[1],
            year: parsedParts[2],
        };
        if (isNaN(mdy.month) || mdy.month < 1 || mdy.month > 12) {
            throw new Error(`NO_REVIEW_REQUESTS_ON_THESE_DATES date "${dateString}" has invalid month`);
        }
        if (isNaN(mdy.date) || mdy.date < 1 || mdy.date > 31) {
            throw new Error(`NO_REVIEW_REQUESTS_ON_THESE_DATES date "${dateString}" has invalid date`);
        }
        if (isNaN(mdy.year) || mdy.year < 0 || mdy.year > 99) {
            throw new Error(`NO_REVIEW_REQUESTS_ON_THESE_DATES date "${dateString}" has invalid year`);
        }
        return mdy;
    })
);

/* Ensure MIN_HOURS_BETWEEN_REVIEW_REQUESTS */

let MIN_HOURS_BETWEEN_REVIEW_REQUESTS = Number(injectedEnv.MIN_HOURS_BETWEEN_REVIEW_REQUESTS || 0);

if (Number.isNaN(MIN_HOURS_BETWEEN_REVIEW_REQUESTS)) {
    MIN_HOURS_BETWEEN_REVIEW_REQUESTS = 0;
}

/* Ensure INTERVAL_SECONDS */

if (!injectedEnv.INTERVAL_SECONDS) {
    throw new Error('INTERVAL_SECONDS must be set');
}

const INTERVAL_SECONDS = Number(injectedEnv.INTERVAL_SECONDS);

if (Number.isNaN(INTERVAL_SECONDS)) {
    throw new Error('INTERVAL_SECONDS must be a number');
}

/* Ensure FAIL_RETRY_ATTEMPTS */

let FAIL_RETRY_ATTEMPTS = Number(injectedEnv.FAIL_RETRY_ATTEMPTS || 0);

if (Number.isNaN(FAIL_RETRY_ATTEMPTS)) {
    FAIL_RETRY_ATTEMPTS = 0;
}

export const env: Env = {
    GITLAB_HOST,
    GITLAB_PROJECT_ID,
    GITLAB_TOKEN,
    SLACK_WEBHOOK_URL,
    SLACK_ERROR_MONIT_CHANNEL,
    SLACK_REVIEW_REQ_CHANNEL,
    SLACK_REVIEW_REQ_REVIEWERS,
    AM_REMINDER_HOUR,
    AM_REMINDER_MIN,
    PM_REMINDER_HOUR,
    PM_REMINDER_MIN,
    ENABLE_RENOVATE_BOT,
    ENABLE_WEEKEND_REVIEW_REQUESTS,
    NO_REVIEW_REQUESTS_ON_THESE_DATES,
    MIN_HOURS_BETWEEN_REVIEW_REQUESTS,
    INTERVAL_SECONDS,
    FAIL_RETRY_ATTEMPTS,
};
