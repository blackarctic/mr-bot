import path from 'path';
import low from 'lowdb';
import FileSync from 'lowdb/adapters/FileSync';
import { env } from '../env';

const dbPath = path.join(__dirname, '..', '..', 'db.json');
const dbAdapter = new FileSync(dbPath);
export const db = low(dbAdapter);

const {
    GITLAB_HOST,
    GITLAB_PROJECT_ID,
} = env;

// Set some defaults (required if your JSON file is empty)
db.defaults({
    mrs: {},
    projectId: null,
    host: null,
    lastRunAt: null,
    reviewReminderAmSent: false,
    reviewReminderPmSent: false,
}).write();

// Verify the host and projectId match the db values
const host = db.get('host').value();
const projectId = db.get('projectId').value();
// Otherwise, we need to clear the data as it doesn't apply to this project
if (host !== GITLAB_HOST || projectId !== GITLAB_PROJECT_ID) {
    db.set('mrs', {}).write();
}
// Finally, set the host and projectId to the current values
db.set('host', GITLAB_HOST).write();
db.set('projectId', GITLAB_PROJECT_ID).write();
